{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "application/vnd.databricks.v1+cell": {
     "cellMetadata": {},
     "inputWidgets": {},
     "nuid": "f97e1784-7790-4600-8333-a0172f0cdc07",
     "showTitle": false,
     "title": ""
    }
   },
   "source": [
    "# Entity Extraction with Claude\n",
    "\n",
    "> *This notebook should work well with the **`Data Science 3.0`** kernel in SageMaker Studio*\n",
    "\n",
    "### Context\n",
    "Entity extraction is an NLP technique that allows us to automatically extract specific data from naturally written text, such as news, emails, books, etc.\n",
    "That data can then later be saved to a database, used for lookup or any other type of processing.\n",
    "\n",
    "Classic entity extraction programs usually limit you to pre-defined classes, such as name, address, price, etc. or require you to provide many examples of types of entities you are interested in.\n",
    "By using a LLM for entity extraction in most cases you are only required to specify what you need to extract in natural language. This gives you flexibility and accuracy in your queries while saving time by removing necessity of data labeling.\n",
    "\n",
    "In addition, LLM entity extraction can be used to help you assemble a dataset to later create a customised solution for your use case, such as [Amazon Comprehend custom entity](https://docs.aws.amazon.com/comprehend/latest/dg/custom-entity-recognition.html) recognition."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "application/vnd.databricks.v1+cell": {
     "cellMetadata": {},
     "inputWidgets": {},
     "nuid": "b66da70c-1620-4117-a2c7-c0125e97bc2a",
     "showTitle": false,
     "title": ""
    }
   },
   "source": [
    "## Setup\n",
    "\n",
    "⚠️ ⚠️ ⚠️ Before running this notebook, ensure you've run the [Bedrock boto3 setup notebook](../00_Intro/bedrock_boto3_setup.ipynb#Prerequisites) notebook. ⚠️ ⚠️ ⚠️\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {
    "application/vnd.databricks.v1+cell": {
     "cellMetadata": {},
     "inputWidgets": {},
     "nuid": "c55e393e-60eb-4f26-b857-f83a509d4ab8",
     "showTitle": false,
     "title": ""
    }
   },
   "outputs": [],
   "source": [
    "import os\n",
    "import sys\n",
    "\n",
    "module_path = \"..\"\n",
    "sys.path.append(os.path.abspath(module_path))\n",
    "from utils import bedrock\n",
    "\n",
    "\n",
    "# ---- ⚠️ Un-comment and edit the below lines as needed for your AWS setup ⚠️ ----\n",
    "\n",
    "# os.environ[\"AWS_DEFAULT_REGION\"] = \"<REGION_NAME>\"  # E.g. \"us-east-1\"\n",
    "# os.environ[\"AWS_PROFILE\"] = \"<YOUR_PROFILE>\"\n",
    "# os.environ[\"BEDROCK_ASSUME_ROLE\"] = \"<YOUR_ROLE_ARN>\"  # E.g. \"arn:aws:...\"\n",
    "\n",
    "\n",
    "boto3_bedrock = bedrock.get_bedrock_client(\n",
    "    assumed_role=os.environ.get(\"BEDROCK_ASSUME_ROLE\", None),\n",
    "    region=os.environ.get(\"AWS_DEFAULT_REGION\", None),\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "application/vnd.databricks.v1+cell": {
     "cellMetadata": {},
     "inputWidgets": {},
     "nuid": "303fdc54-84ec-48e7-a81f-55e0457235ef",
     "showTitle": false,
     "title": ""
    }
   },
   "source": [
    "## Configure langchain\n",
    "\n",
    "We begin with instantiating the LLM. Here we are using Anthropic Claude v2 for text generation.\n",
    "\n",
    "Note: It is possible to choose other models available with Bedrock. You can replace the `model_id` as follows to change the model.\n",
    "\n",
    "`llm = Bedrock(model_id=\"amazon.titan-tg1-large\")`\n",
    "\n",
    "Check [documentation](https://docs.aws.amazon.com/bedrock/latest/userguide/model-ids-arns.html) for Available text generation model Ids under Amazon Bedrock."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {
    "application/vnd.databricks.v1+cell": {
     "cellMetadata": {},
     "inputWidgets": {},
     "nuid": "0365bb97-e042-4b1a-9ba0-9dfe60789bcb",
     "showTitle": false,
     "title": ""
    }
   },
   "outputs": [],
   "source": [
    "from langchain.llms.bedrock import Bedrock\n",
    "\n",
    "# - create the Anthropic Model\n",
    "llm = Bedrock(\n",
    "    model_id=\"anthropic.claude-v2\",\n",
    "    client=boto3_bedrock,\n",
    "    model_kwargs={\n",
    "        \"max_tokens_to_sample\": 200,\n",
    "        \"temperature\": 0, # Using 0 to get reproducible results\n",
    "        \"stop_sequences\": [\"\\n\\nHuman:\"]\n",
    "    }\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "application/vnd.databricks.v1+cell": {
     "cellMetadata": {},
     "inputWidgets": {},
     "nuid": "73c7807f-03cd-43b8-b8af-405be2dcf013",
     "showTitle": false,
     "title": ""
    }
   },
   "source": [
    "## Entity Extraction\n",
    "Now that we have our LLM initialised, we can start extracting entities.\n",
    "\n",
    "For this exercise we will pretend to be an online bookstore that receives questions and orders by email.\n",
    "Our task would be to extract relevant information from the email to process the order.\n",
    "\n",
    "Let's begin by taking a look at the sample email:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {
    "application/vnd.databricks.v1+cell": {
     "cellMetadata": {},
     "inputWidgets": {},
     "nuid": "118ac3bd-879b-48e9-af66-c6087a99c360",
     "showTitle": false,
     "title": ""
    }
   },
   "outputs": [],
   "source": [
    "from pathlib import Path\n",
    "\n",
    "emails_dir = Path(\".\") / \"emails\"\n",
    "with open(emails_dir / \"00_treasure_island.txt\") as f:\n",
    "    book_question_email = f.read()\n",
    "\n",
    "print(book_question_email)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "application/vnd.databricks.v1+cell": {
     "cellMetadata": {},
     "inputWidgets": {},
     "nuid": "fb4dd995-c6e0-41fe-8dcd-b7b0a00c7ed3",
     "showTitle": false,
     "title": ""
    }
   },
   "source": [
    "### Basic approach\n",
    "\n",
    "For basic cases we can directly ask the model to return the result.\n",
    "Let's try extracting the name of the book."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {
    "application/vnd.databricks.v1+cell": {
     "cellMetadata": {},
     "inputWidgets": {},
     "nuid": "782ae75d-8886-4b11-a456-d058edd8b60c",
     "showTitle": false,
     "title": ""
    }
   },
   "outputs": [],
   "source": [
    "query = f\"\"\"\n",
    "\n",
    "Human: Given the email inside triple-backticks, please read it and analyse the contents.\n",
    "If a name of a book is mentioned, return it, otherwise return nothing.\n",
    "\n",
    "Email: ```\n",
    "{book_question_email}\n",
    "```\n",
    "\n",
    "Assistant:\"\"\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {
    "application/vnd.databricks.v1+cell": {
     "cellMetadata": {},
     "inputWidgets": {},
     "nuid": "57c4e139-773e-496f-bda4-4197b6ed6d86",
     "showTitle": false,
     "title": ""
    }
   },
   "outputs": [],
   "source": [
    "result = llm(query)\n",
    "print(result.strip())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "application/vnd.databricks.v1+cell": {
     "cellMetadata": {},
     "inputWidgets": {},
     "nuid": "ba0f82df-0001-4610-987c-ed5179d18a7f",
     "showTitle": false,
     "title": ""
    }
   },
   "source": [
    "### Model specific prompts\n",
    "\n",
    "While basic approach works, to achieve best results we recommend to customise your prompts for the particular model you will be using.\n",
    "In this example we are using `anthropic.claude-v2`, [prompt guide for which can be found here](https://docs.anthropic.com/claude/docs/introduction-to-prompt-design).\n",
    "\n",
    "Here is the a more optimised prompt for Claude v2."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {
    "application/vnd.databricks.v1+cell": {
     "cellMetadata": {},
     "inputWidgets": {},
     "nuid": "310282d5-6461-4f75-a398-878239580b09",
     "showTitle": false,
     "title": ""
    }
   },
   "outputs": [],
   "source": [
    "prompt = \"\"\"\n",
    "\n",
    "Human: Given the email provided, please read it and analyse the contents.\n",
    "If a name of a book is mentioned, return it.\n",
    "If no name is mentioned, return empty string.\n",
    "The email will be given between <email></email> XML tags.\n",
    "\n",
    "<email>\n",
    "{email}\n",
    "</email>\n",
    "\n",
    "Return the name of the book between <book></book> XML tags.\n",
    "\n",
    "Assistant:\"\"\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {
    "application/vnd.databricks.v1+cell": {
     "cellMetadata": {},
     "inputWidgets": {},
     "nuid": "45f6ed14-8878-49f9-b337-8d9aea3b13cb",
     "showTitle": false,
     "title": ""
    }
   },
   "outputs": [],
   "source": [
    "query = prompt.format(email=book_question_email)\n",
    "result = llm(query)\n",
    "print(result.strip())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "application/vnd.databricks.v1+cell": {
     "cellMetadata": {},
     "inputWidgets": {},
     "nuid": "7ce6c73f-0d22-4219-a2e9-ab9b59b63d5c",
     "showTitle": false,
     "title": ""
    }
   },
   "source": [
    "To extract results easier, we can use a helper function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {
    "application/vnd.databricks.v1+cell": {
     "cellMetadata": {},
     "inputWidgets": {},
     "nuid": "77d55dd4-6f37-45e8-90ff-fb6e2ccdec65",
     "showTitle": false,
     "title": ""
    }
   },
   "outputs": [],
   "source": [
    "from bs4 import BeautifulSoup\n",
    "\n",
    "def extract_by_tag(response: str, tag: str, extract_all=False) -> str | list[str] | None:\n",
    "    soup = BeautifulSoup(response)\n",
    "    results = soup.find_all(tag)\n",
    "    if not results:\n",
    "        return\n",
    "        \n",
    "    texts = [res.get_text() for res in results]\n",
    "    if extract_all:\n",
    "        return texts\n",
    "    return texts[-1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {
    "application/vnd.databricks.v1+cell": {
     "cellMetadata": {},
     "inputWidgets": {},
     "nuid": "ef545d74-f1d1-4773-a627-816242af1bf7",
     "showTitle": false,
     "title": ""
    }
   },
   "outputs": [],
   "source": [
    "extract_by_tag(result, \"book\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "application/vnd.databricks.v1+cell": {
     "cellMetadata": {},
     "inputWidgets": {},
     "nuid": "aad494b4-022a-4f6b-aac4-fd1cdd128ca0",
     "showTitle": false,
     "title": ""
    }
   },
   "source": [
    "We can check that our model doesn't return arbitrary results when no appropriate information is given (also know as 'hallucination'), by running our prompt on other emails."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {
    "application/vnd.databricks.v1+cell": {
     "cellMetadata": {},
     "inputWidgets": {},
     "nuid": "5bf93ba6-7939-499d-acb4-1e576a9576d7",
     "showTitle": false,
     "title": ""
    }
   },
   "outputs": [],
   "source": [
    "with open(emails_dir / \"01_return.txt\") as f:\n",
    "    return_email = f.read()\n",
    "\n",
    "print(return_email)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {
    "application/vnd.databricks.v1+cell": {
     "cellMetadata": {},
     "inputWidgets": {},
     "nuid": "94f19028-cf0c-4359-b7d5-8643d5044aed",
     "showTitle": false,
     "title": ""
    }
   },
   "outputs": [],
   "source": [
    "query = prompt.format(email=return_email)\n",
    "result = llm(query)\n",
    "print(result.strip())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "application/vnd.databricks.v1+cell": {
     "cellMetadata": {},
     "inputWidgets": {},
     "nuid": "e262da99-184a-4fc1-84cc-0ba361170772",
     "showTitle": false,
     "title": ""
    }
   },
   "source": [
    "Using tags also allows us to extract multiple pieces of information at the same time and makes extraction much easier.\n",
    "In the following prompt we will extract not just the book name, but any questions, requests and customer name."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {
    "application/vnd.databricks.v1+cell": {
     "cellMetadata": {},
     "inputWidgets": {},
     "nuid": "21b6c997-b9c2-4282-bf0b-25b2d60542af",
     "showTitle": false,
     "title": ""
    }
   },
   "outputs": [],
   "source": [
    "prompt = \"\"\"\n",
    "\n",
    "Human: Given email provided , please read it and analyse the contents.\n",
    "\n",
    "Please extract the following information from the email:\n",
    "- Any questions the customer is asking, return it inside <questions></questions> XML tags.\n",
    "- The customer full name, return it inside <name></name> XML tags.\n",
    "- Any book names the customer mentions, return it inside <books></books> XML tags.\n",
    "\n",
    "If a particular bit of information is not present, return an empty string.\n",
    "Make sure that each question can be understoon by itself, incorporate context if requred.\n",
    "Each returned question should be concise, remove extra information if possible.\n",
    "The email will be given between <email></email> XML tags.\n",
    "\n",
    "<email>\n",
    "{email}\n",
    "</email>\n",
    "\n",
    "Return each question inside <question></question> XML tags.\n",
    "Return the name of each book inside <book></book> XML tags.\n",
    "\n",
    "Assistant:\"\"\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {
    "application/vnd.databricks.v1+cell": {
     "cellMetadata": {},
     "inputWidgets": {},
     "nuid": "0009b803-a286-43f4-a37f-d32e15daaf31",
     "showTitle": false,
     "title": ""
    }
   },
   "outputs": [],
   "source": [
    "query = prompt.format(email=book_question_email)\n",
    "result = llm(query)\n",
    "print(result.strip())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {
    "application/vnd.databricks.v1+cell": {
     "cellMetadata": {},
     "inputWidgets": {},
     "nuid": "f8894ea6-8756-4a33-8a09-d107353e463a",
     "showTitle": false,
     "title": ""
    }
   },
   "outputs": [],
   "source": [
    "extract_by_tag(result, \"question\", extract_all=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {
    "application/vnd.databricks.v1+cell": {
     "cellMetadata": {},
     "inputWidgets": {},
     "nuid": "3a5bf608-9571-45b1-99bd-574076fd73c2",
     "showTitle": false,
     "title": ""
    }
   },
   "outputs": [],
   "source": [
    "extract_by_tag(result, \"name\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {
    "application/vnd.databricks.v1+cell": {
     "cellMetadata": {},
     "inputWidgets": {},
     "nuid": "cd62c0cb-c7d4-4a10-9e9d-b20f5362fa87",
     "showTitle": false,
     "title": ""
    }
   },
   "outputs": [],
   "source": [
    "extract_by_tag(result, \"book\", extract_all=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "application/vnd.databricks.v1+cell": {
     "cellMetadata": {},
     "inputWidgets": {},
     "nuid": "7a7d4e3a-541f-4658-adf9-c1ab7026a3ce",
     "showTitle": false,
     "title": ""
    }
   },
   "source": [
    "## Conclusion\n",
    "\n",
    "Entity extraction is a powerful technique using which you can extract arbitrary data using plain text descriptions.\n",
    "\n",
    "This is particularly useful when you need to extract specific data which doesn't have clear structure. In such cases regex and other traditional extraction techniques can be very difficult to implement.\n",
    "\n",
    "### Take aways\n",
    "- Adapt this notebook to experiment with different models available through Amazon Bedrock such as Amazon Titan and AI21 Labs Jurassic models.\n",
    "- Change the prompts to your specific usecase and evaluate the output of different models.\n",
    "- Apply different prompt engineering principles to get better outputs. Refer to the prompt guide for your chosen model for recommendations, e.g. [here is the prompt guide for Claude](https://docs.anthropic.com/claude/docs/introduction-to-prompt-design)."
   ]
  }
 ],
 "metadata": {
  "application/vnd.databricks.v1+notebook": {
   "dashboards": [],
   "language": "python",
   "notebookMetadata": {},
   "notebookName": "entity_extraction",
   "widgets": {}
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.4"
  },
  "vscode": {
   "interpreter": {
    "hash": "00878cbed564b904a98b4a19808853cb6b9988746b881ea025a8408713879bf5"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
